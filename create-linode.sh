#!/usr/bin/bash

set -eo pipefail

host="$1"

# avoid unbound variable if LINODE_USER is empty
impossible_username="_~!unset!~_"
if [ "${LINODE_USER:-$impossible_username}" == "$impossible_username" ]; then
  echo "Please specify LINODE_USER, e.g.:"
  echo
  printf "\t\$ LINODE_USER=nadia bash $0 $1" && echo
  printf "\t\tor" && echo
  printf "\t\$ export LINODE_USER=sax" && echo
  printf "\t\$ bash $0 $1" && echo
  exit
fi
user="$LINODE_USER"

type="g6-nanode-1"
region="ca-central"

red=$(tput setaf 5)
off=$(tput sgr0)

# Verify that there is not a naming conflict with chosen host name
status_code=$(curl --write-out %{http_code} --silent --output /dev/null "https://gitlab.com/api/v4/projects/17452903/repository/files/hosts%2F$host%2Enix?ref=master")

if [[ "$status_code" -eq 200 ]] ; then
  echo "There is already a host named $host. Please run the script again with a different host name." && exit 1
fi

# Select the most recent image on this account with "nixos" in the label
imageid=$(linode-cli images list --text --format 'id,label' | grep private | grep -i nixos | cut -f1 | tail -n1) &&
imagelabel=$(linode-cli images list --text --format 'id,label' | grep private | grep -i nixos | cut -f2 | tail -n1) &&

# https://anarc.at/blog/2017-02-18-passwords-entropy/
rootpass=$(tr -dc 'A-Za-z0-9' < /dev/urandom | head -c 24) &&
echo " "
echo "If the install succeeds, the root password will be ${red}$rootpass${off}"
echo " "

echo "Creating new $host node with NixOS image $imagelabel"
echo " "
linode-cli linodes create --type "$type" --region $region --label $host --image "$imageid" --booted false --root_pass "$rootpass" --authorized_users "$user" && 
echo " " && sleep 20 &&

nodeid=$(linode-cli linodes list --text --format 'id,label' | grep $host | cut -f1 | tail -n1) && sleep 5 &&

echo "If this script seems to fail, run this command to clean up the node before retrying:"
echo "${red}linode-cli linodes delete $nodeid${off}"
echo " "

configid=$(linode-cli linodes configs-list "$nodeid" --text --format 'id' | tail -n1) && sleep 3 &&
ip=$(linode-cli linodes list --text --format "id,ipv4"| grep "$nodeid" | cut -f2) && sleep 3 &&

echo "Configuring node to boot NixOS supplied kernel using GRUB2."
echo " "

linode-cli linodes config-update --kernel linode/grub2 "$nodeid" "$configid" && sleep 5 &&

echo " "
echo "Expanding disk to fill remaining storage capacity of $type." && sleep 10 &&
echo " "
capacity=$(linode-cli linodes type-view "$type" --text --format 'disk' | tail -n1) && sleep 3 &&
swapsize=$(linode-cli linodes disks-list "$nodeid" --text --format 'label,size' | grep -i swap | cut -f2) && sleep 3 &&
rootsize=$(linode-cli linodes disks-list "$nodeid" --text --format 'label,size' | grep -i nixos | cut -f2) && sleep 3 &&
rootgoal=$((capacity-swapsize))
rootid=$(linode-cli linodes disks-list "$nodeid" --text --format 'id,label' | grep nixos | cut -f1) && sleep 3 &&

until [[ "$rootsize" == "$rootgoal" ]]
do
	rootsize=$(linode-cli linodes disks-list "$nodeid" --text --format 'label,size' | grep -i nixos | cut -f2) && sleep 5
done

echo "Booting new node..." && sleep 20 &&
linode-cli linodes reboot "$nodeid" && sleep 20 &&
echo " "

echo "Adding host key fingerprint from image description to local SSH known_hosts file."
hostkeyfp=$(linode-cli images list --text --format 'label,description' | grep nixos | tail -n1 | cut -f2)
echo "$ip ssh-ed25519 $hostkeyfp" >> ~/.ssh/known_hosts
echo " "

echo "Waiting 20 seconds to make sure SSH has come online after reboot."
sleep 20 &&
echo " "

echo "Test if the SSH connection works..."
ssh "root@$ip" exit &&
echo " "
read -p "Press enter to continue if SSH successfully connected."
echo " "

echo "Installing git."
echo " "
ssh "root@$ip" "nix-env -f '<nixpkgs>' -iA git" &&
echo " "

echo "Creating a git repo in /etc/nixos and syncing it to https://gitlab.com/distrosync/nixos.git"
echo " "
ssh "root@$ip" "cd /etc/nixos && git init . && git remote add origin https://gitlab.com/distrosync/nixos.git && git pull origin master" &&
echo " "

echo "Creating /etc/nixos/hosts/$host.nix based on /etc/nixos/hosts/template.nix"
ssh "root@$ip" "cd /etc/nixos/hosts && cp template.nix $host.nix && sed -i 's/TMPLT/$host/g' $host.nix" &&
echo " "

echo "Replacing the default configuration.nix with a symlink to /etc/hosts/$host.nix"
ssh "root@$ip" "rm /etc/nixos/configuration.nix && ln -s /etc/nixos/hosts/$host.nix /etc/nixos/configuration.nix" &&
echo " "

echo "Rebuilding NixOS with the a basic DistroSync node configuration."
echo " "
ssh "root@$ip" "nixos-rebuild switch"
echo " "

echo "You can now login to the new $host node at ${red}$ip${off}. The root password is ${red}$rootpass${off}"
echo "The netdata dashboard can be accessed at ${red}http://$ip${off}."
echo " "
